import axios from 'axios';
import firebase from 'firebase';

// Initialize Firebase
const firebaseConfig = {
  apiKey: 'AIzaSyCZ__5gn8Qigt6vTc2BolyQYVK8z_cVzQE',
  authDomain: 'challenges-52857.firebaseapp.com',
  databaseURL: 'https://challenges-52857.firebaseio.com',
  projectId: 'challenges-52857',
  storageBucket: 'challenges-52857.appspot.com',
  messagingSenderId: '580782795807',
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
db.settings({ timestampsInSnapshots: true });

let apiUrl;

if (process.env.NODE_ENV === 'production') {
  apiUrl = 'https://jsonplaceholder.typicode.com';
} else {
  apiUrl = 'https://jsonplaceholder.typicode.com';
}

// You may need to use axios interceprors in authentication process. Take a look https://github.com/axios/axios/issues/266

export function fetchUser(id) {
  return axios.get(`${apiUrl}/users/${id}`).then(response => response);
}

export function updateProgress(userID, challengeID, challenge) {
  if (userID !== null) {
    return db
      .collection('users')
      .doc(userID)
      .set(
        {
          [challengeID]: JSON.stringify(challenge),
          lastUpdate: challenge.updatedAt,
        },
        { merge: true },
      );
  }
  return db.collection('users').add({
    [challengeID]: JSON.stringify(challenge),
    lastUpdate: challenge.updatedAt,
  });
}

export function fetchProgress(userID, challengeID) {
  if (userID === null) {
    return Promise.reject();
  }
  return db
    .collection('users')
    .doc(userID)
    .get()
    .then(doc => {
      if (doc.exists && doc.data()[challengeID]) {
        return Promise.resolve(JSON.parse(doc.data()[challengeID]));
      }
      // doc.data() will be undefined in this case
      // eslint-disable-next-line no-console
      console.log('No such document!');
      return false;
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log('Error getting document:', error);
    });
}

export const challenges = [
  {
    id: 1,
    slug: 'cookies',
    name: 'A week without cookies',
    icon: 'gingerbread-man',
    prosCons: 'Most pastries, cookies and cakes are extremely unhealthy.\n\n',
    checklist: ['Sugar causes pimples', 'Put on weight'],
  },
  {
    id: 2,
    slug: 'on-time-to-work',
    name: 'To be on time to work',
    icon: 'on-time-to-work',
    prosCons: '',
  },
  {
    id: 3,
    slug: 'go-to-bed',
    name: 'Go to bed on time',
    icon: 'bed',
    prosCons:
      'https://medium.com/s/story/combat-tested-training-unwind-and-sleep-anywhere-in-120-seconds-27d5307b7606',
  },
  {
    id: 4,
    slug: 'wake-up',
    name: 'Wake up early',
    icon: 'wake-up',
    prosCons: '',
  },
  {
    id: 5,
    slug: 'fresh-shower',
    name: 'Fresh shower every morning',
    icon: 'shower',
    prosCons: '',
  },
  {
    id: 6,
    slug: 'morning-workout',
    name: 'Morning workout',
    icon: 'morning-workout',
    prosCons: '',
  },
];

export const modalText =
  "Hello, my name is Andrey. I like to make my life happier. I very glad to fill your life of little victories. Let's go this way together!";
