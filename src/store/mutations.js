export const SET_USER = 'FETCH_USER';
export const SET_CHALLENGE = 'SET_CHALLENGE';
export const SET_USER_ID = 'SET_USER_ID';

export default {
  [SET_USER]: (state, user) => {
    state.user = user;
  },

  [SET_CHALLENGE]: (state, { challengeID, challenge }) => {
    let flag = false;

    state.user.challenges = state.user.challenges.map(e => {
      if (e.id === challengeID) {
        flag = true;
        return challenge;
      }
      return e;
    });

    if (!flag && challenge !== undefined) {
      state.user.challenges.push(challenge);
    }
  },

  [SET_USER_ID]: (state, id) => {
    localStorage.setItem('myID', id);
    state.user.id = id;
  },
};
