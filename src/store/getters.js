export const GET_USER = 'GET_USER';
export const GET_CHALLENGE = 'GET_CHALLENGE';

export default {
  [GET_USER]: state => {
    const { user } = state;
    return { ...user };
  },

  [GET_CHALLENGE]: state => id => {
    const { user } = state;
    return user.challenges.filter(e => e.id === id)[0];
  },
};
