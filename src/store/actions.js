import { SET_USER, SET_CHALLENGE, SET_USER_ID } from '@/store/mutations';

import { fetchUser, updateProgress, fetchProgress } from '@/data/';

export const FETCH_USER = 'FETCH_USER';
export const UPDATE_CHALLENGE = 'UPDATE_CHALLENGE';
export const FETCH_CHALLENGE = 'FETCH_CHALLENGE';

export default {
  [FETCH_USER]: ({ commit }, id) =>
    fetchUser(id).then(user => commit(SET_USER, { user })),

  [FETCH_CHALLENGE]: ({ commit }, challengeID) => {
    const userID = localStorage.getItem('myID') || null;
    return fetchProgress(userID, challengeID).then(challenge =>
      commit(SET_CHALLENGE, { challengeID, challenge }),
    );
  },

  [UPDATE_CHALLENGE]: ({ commit }, { challengeID, challenge }) => {
    const userID = localStorage.getItem('myID') || null;

    updateProgress(userID, challengeID, challenge).then(docRef => {
      commit(SET_USER_ID, userID || docRef.id);
      commit(SET_CHALLENGE, { challengeID, challenge });
    });
  },
};
