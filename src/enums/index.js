export const weekdays = [
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday',
  'sunday',
];

export const emptyChallenge = {
  id: null,
  startDay: null,
  updatedAt: null,
  progress: {
    monday: null,
    tuesday: null,
    wednesday: null,
    thursday: null,
    friday: null,
    saturday: null,
    sunday: null,
  },
  counters: {
    attempts: 0,
    retakes: 0,
  },
};
