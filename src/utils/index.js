import { weekdays } from '../enums/index';

export function recountWeekdays(startDay) {
  const first = weekdays.filter(
    e => weekdays.indexOf(e) >= weekdays.indexOf(startDay),
  );

  const second = weekdays.filter(
    e => weekdays.indexOf(e) < weekdays.indexOf(startDay),
  );

  return [...first, ...second];
}
