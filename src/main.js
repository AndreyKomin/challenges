import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './router';
import App from './App.vue';
import createStore from './store';

Vue.use(VueRouter);

Vue.config.productionTip = false;

const router = new VueRouter({
  mode: 'history',
  routes,
});

const store = createStore();

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app');
