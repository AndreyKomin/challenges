import Intro from '@/components/Intro.vue';
import Challenge from '@/components/Challenge.vue';
import Challenges from '@/components/Challenges.vue';
import Profile from '@/components/Profile.vue';

export default [
  {
    path: '/',
    component: Intro,
  },
  {
    path: '/challenge/:id',
    component: Challenge,
  },
  {
    path: '/challenges',
    name: 'challenges',
    component: Challenges,
  },
  {
    path: '/Profile',
    name: 'profile',
    component: Profile,
  },
];
