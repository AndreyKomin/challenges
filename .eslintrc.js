// https://eslint.org/docs/user-guide/configuring
const path = require('path')

function resolve (dir) {
  return path.join(__dirname, '', dir)
}

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
    ecmaFeatures: {
      modules: true,
      experimentalObjectRestSpread: true
    }
  },
  env: {
    es6: true,
    node: true,
    browser: true,
  },
  extends: [
    'airbnb-base',
    'prettier',
    'plugin:vue/base',
    'plugin:vue/essential',
    'plugin:vue/strongly-recommended',
    'plugin:vue/recommended'
  ],
  plugins: [
    'vue',
    'prettier',
    'eslint-comments'
  ],
  settings: {
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            alias: {
              vue: 'vue/dist/vue.js',
              '@': resolve('src'),
            },
            modules: [
              'node_modules'
            ]
          },
        }
      }
    }
  },
  rules: {
    'no-param-reassign': ['error', {
      props: true,
      ignorePropertyModificationsFor: [
        'state', // for vuex state
        'acc', // for reduce accumulators
        'e' // for e.returnvalue
      ]
    }],
    'prettier/prettier': ['error',
      {
        printWidth: 80,
        tabWidth: 2,
        semi: true,
        trailingComma: 'all',
        bracketSpacing: true,
        singleQuote: true
      }
    ],
    'import/no-extraneous-dependencies': ['error', {
      optionalDependencies: ['test/unit/index.js']
    }],
    'vue/require-default-prop': 'off',
    'no-plusplus': 'off',
    'no-shadow': 'off',
    'no-restricted-globals': 'off',
    'func-names': 'off',
    'linebreak-style': ['off'],
    'no-implicit-coercion': 'error',
    'import/no-named-as-default': 'off',
    'import/newline-after-import': 'error',
    'eslint-comments/disable-enable-pair': 'error',
    'eslint-comments/no-duplicate-disable': 'error',
    'eslint-comments/no-unlimited-disable': 'error',
    'eslint-comments/no-unused-disable': 'warn',
    'eslint-comments/no-unused-enable': 'warn',

    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  }
};
